FactoryBot.define do
  factory :user do
    firstname { Faker::Name.first_name }
    lastname { Faker::Name.last_name }
    username { Faker::Internet.email }
    password Faker::Internet.password(min_length: 14)
    role { Faker::Number.between(from: 0, to: 1) }
  end
end

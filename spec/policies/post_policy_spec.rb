require "rails_helper"

RSpec.describe PostPolicy, type: :policy do
  let(:admin) { create(:user, role: :admin) }
  let(:job_seaker) { create(:user, role: :job_seaker) }
  let(:job_post) { create(:post) }
  subject { described_class }

  #   permissions ".scope" do
  #     pending "add some examples to (or delete) #{__FILE__}"
  #   end

  permissions :show? do
    it "Job posts requested" do
      expect(subject).to permit(nil, job_post)
    end
  end

  permissions :create? do
    it "Job Seeker" do
      expect(subject).not_to permit(job_seaker, job_post)
    end
    it "Admin User" do
      expect(subject).to permit(admin, job_post)
    end
  end

  permissions :update? do
    it "Job Seeker" do
      expect(subject).not_to permit(job_seaker, job_post)
    end
    it "Admin User" do
      expect(subject).to permit(admin, job_post)
    end
  end

  permissions :destroy? do
    it "Job Seeker" do
      expect(subject).not_to permit(job_seaker, job_post)
    end
    it "Admin User" do
      expect(subject).to permit(admin, job_post)
    end
  end
end

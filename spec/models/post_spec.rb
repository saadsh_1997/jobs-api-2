require "rails_helper"

RSpec.describe Post, type: :model do
  it { should have_many(:job_applications).dependent(:destroy) }
  it { should validate_presence_of(:created_by) }
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:body) }
end

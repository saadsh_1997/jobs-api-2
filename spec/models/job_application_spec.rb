require "rails_helper"

RSpec.describe JobApplication, type: :model do
  it { should belong_to(:post) }
  it { should validate_presence_of(:status) }
  it { should validate_presence_of(:created_by) }
end

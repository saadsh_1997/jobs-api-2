class CreateJobApplications < ActiveRecord::Migration[6.1]
  def change
    create_table :job_applications do |t|
      t.string :created_by
      t.references :post, null: false, foreign_key: true
      t.integer :status

      t.timestamps
    end
  end
end

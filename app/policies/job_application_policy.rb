class JobApplicationPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    true
  end

  def create?
    @user.role_job_seaker?
  end

  def update?
    @user.role_job_seaker?
  end

  def destroy?
    @user.role_job_seaker?
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end

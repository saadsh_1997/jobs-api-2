class JobApplicationsController < ApplicationController
  before_action :set_post
  before_action :set_post_job_application, only: [:show, :update, :destroy]

  # GET /posts/:post_id/job_applications
  def index
    @job_applications = @post.job_applications
    json_response(@job_applications)
  end

  # GET /posts/:post_id/job_applications/:id
  def show
    json_response(@job_application)
    authorize JobApplication
  end

  # POST /posts/:post_id/job_applications
  def create
    authorize JobApplication
    @job_application = @post.job_applications.new(job_application_params)
    @job_application.created_by = current_user.id
    @job_application.save!
    json_response(@post, :created)
  end

  # PUT /posts/:post_id/job_applications/:id
  def update
    authorize JobApplication
    @job_application.update(job_application_params)
    head :no_content
  end

  # DELETE /posts/:post_id/job_applications/:id
  def destroy
    authorize JobApplication
    @job_application.destroy
    head :no_content
  end

  private

  def job_application_params
    params.permit(:status)
  end

  def set_post
    @post = Post.find(params[:post_id])
  end

  def set_post_job_application
    @job_application = @post.job_applications.find_by!(id: params[:id]) if @post
  end
end
